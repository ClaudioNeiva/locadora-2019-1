package br.ucsal.testequalidade20191.locadora.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.testequalidade20191.locadora.dominio.Locacao;
import br.ucsal.testequalidade20191.locadora.exception.LocacaoNaoEncontradaException;

public class LocacaoDAO {

	private List<Locacao> locacoes = new ArrayList<>();

	public Locacao obterPorNumeroContrato(Integer numeroContrato) throws LocacaoNaoEncontradaException {
		for (Locacao locacao : locacoes) {
			if (locacao.getNumeroContrato().equals(numeroContrato)) {
				return locacao;
			}
		}
		throw new LocacaoNaoEncontradaException();
	}

	public void insert(Locacao locacao) {
		locacoes.add(locacao);
	}

}
